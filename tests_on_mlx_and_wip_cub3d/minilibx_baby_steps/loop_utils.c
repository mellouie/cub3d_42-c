#include "mlx/mlx.h"
#include <math.h>
#include <stdlib.h>
#include "loop.h"
#include <string.h>


void		background(t_mlx *mlx, int color)
{
	int count_x = -1;
	int count_y = -1;
	while (++count_x <= WIDTH)											//remplissage
	{
		while (++count_y <= HEIGHT)
			mlx->img.data[(count_y * WIDTH + count_x )] = color;
		count_y = 0;
	}
}

void	print_rectangle(int xstart, int ystart, int width, int heigth, int color, t_mlx *mlx)
{
	int x;
	int y;

	x = xstart;
	y = ystart;
	while (y < heigth + ystart)
	{
		while (x < width + xstart)
		{
			mlx->img.data[(y * WIDTH + x )] = color;			
			x++;
		}
		y++;
		x = xstart;
	}
}

static void	draw(t_mlx *mlx)
{
	mlx_clear_window(mlx->mlx, mlx->win);
	mlx_put_image_to_window(mlx->mlx, mlx->win, mlx->img.ptr, 0, 0);
}

int		img_loop(t_mlx *mlx)
{
	int x = -80;
	int y = 0;

	if (x > WIDTH)
	{
		x = 0;
		y += 80;
	}
	if (y > HEIGHT)
		y = 0;
	background(mlx, 0x00FFFFFF);
    print_rectangle(x, y, 80, 80, 0x00FF0000, mlx);
     draw(mlx);
	 return (0);
}

t_img	get_img(void *mlx)
{
	t_img	img;

	img.ptr = mlx_new_image(mlx, WIDTH, HEIGHT);
	img.data = (int *)mlx_get_data_addr(img.ptr, &img.bpp, &img.size_length, &img.endian);
	return (img);
}