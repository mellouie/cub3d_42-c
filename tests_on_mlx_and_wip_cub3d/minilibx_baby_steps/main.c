/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 15:00:43 by mdesfont          #+#    #+#             */
/*   Updated: 2020/06/16 18:59:44 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "mlx/mlx.h"
#include "test.h"


int	main(void)
{
	void	*ptr;
	void	*win;
	char	*str_color;
	int		color;
	int y = -1;
	int x = -1;

	str_color = "F     56,39,248";
	color = ft_rgb_from_str(str_color);
	ptr = mlx_init(); //identifiant de la connexion au serveur graphique
	win = mlx_new_window(ptr, HIGH, WEIGHT, "hello world"); //ouvre une fenêtre - win = id sur la fenêtre crée
	while (++x < 249)
	{
		mlx_pixel_put(ptr, win, x, y, color);
		while (++y < 249)
			mlx_pixel_put(ptr, win, x, y, color);
		y = 0;
	}
	mlx_key_hook(win, deal_key, (void *) 0);
	mlx_loop(ptr);
}
