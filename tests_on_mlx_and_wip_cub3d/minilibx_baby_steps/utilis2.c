/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utilis2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/02 10:41:32 by mdesfont          #+#    #+#             */
/*   Updated: 2020/06/02 10:53:10 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test2.h"

int		ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	else
		return (0);	
}

int		get_color_str(char *str)
{
	int rgb;
	int g;
	int b;
	int i;

	rgb = 0;
	g = 0;
	b = 0;
	i = 0;
	while (!ft_isdigit(str[i]))
		i++;
	while (ft_isdigit(str[i]))
		rgb = rgb * 10 + str[i++] - '0';
	while (!ft_isdigit(str[i]))
		i++;
	while (ft_isdigit(str[i]))
		g = g * 10 + str[i++] - '0';
	while (!ft_isdigit(str[i]))
		i++;
	while (ft_isdigit(str[i]))
		b = b * 10 + str[i++] - '0';
	return (rgb = ((((rgb << 8) + g) << 8) + b));
}