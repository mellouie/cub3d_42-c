/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test2.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/01 12:01:09 by mdesfont          #+#    #+#             */
/*   Updated: 2020/06/03 14:12:03 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST2_H
# define TEST2_H
# define WIDTH 600
# define HEIGHT 400
# define KEY_ESC 53

typedef struct	s_img
{
	void	*img;
	int		*data;
	int		bpp;
	int		size_length;
	int		endian;
}				t_img;

typedef struct	s_mlx
{
	void	*mlx_ptr;
	void	*win_ptr;
	t_img	img;
}				t_mlx;

int		get_color_str(char *str);

#endif