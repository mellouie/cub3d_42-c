#include <unistd.h>
#include <stdio.h>
#include "mlx/mlx.h"
#include <math.h>
#include <stdlib.h>
#include "loop.h"

void	setup_win(t_mlx *mlx)
{
	mlx->mlx = mlx_init();
	mlx->win = mlx_new_window(mlx->mlx, WIDTH, HEIGHT, WIN_NAME);
	mlx->img = get_img(mlx->mlx);
}

int 	main()
{
	t_mlx	mlx;

	setup_win(&mlx);
	mlx_loop_hook(mlx.mlx, img_loop, &mlx);
	mlx_loop(mlx.mlx);
	return (0);
}