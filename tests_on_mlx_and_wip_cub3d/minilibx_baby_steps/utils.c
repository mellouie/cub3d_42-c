/* *************************************************************************/
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/30 14:06:52 by mdesfont          #+#    #+#             */
/*   Updated: 2020/05/31 13:27:47 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* *************************************************************************/

#include <unistd.h>
#include "mlx/mlx.h"
#include "test.h"

int		ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	else
		return (0);
}

int		ft_rgb_from_str(char *string)
{
	int	rgb;
	int g;
	int b;
	int i;

	rgb = 0;
	g = 0;
	b = 0;
	i = 0;
	while (!ft_isdigit(string[i]))
		i++;
	while (ft_isdigit(string[i]))
		rgb = rgb * 10 + string[i++] - '0';
	while (string[i] == ',')
		i++;
	while (ft_isdigit(string[i]))
		g = g * 10 + string[i++] - '0';
	while (string[i] == ',')
		i++;
	while (ft_isdigit(string[i]))
		b = b * 10 + string[i++] - '0';
	return (rgb = (((rgb << 8) + g) << 8) + b);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int	deal_key(int key, void *param)
{
	ft_putchar('i');
	return (0);
}