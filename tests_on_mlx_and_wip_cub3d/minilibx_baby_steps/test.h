/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/30 14:48:29 by mdesfont          #+#    #+#             */
/*   Updated: 2020/05/31 13:56:44 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_H
# define TEST_H
# include "mlx/mlx.h"

#define HIGH 500
#define WEIGHT 500

int		ft_isdigit(int c);
int		ft_rgb_from_str(char *string);
int	deal_key(int key, void *param);
void	ft_putchar(char c);


#endif