/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/29 15:00:43 by mdesfont          #+#    #+#             */
/*   Updated: 2020/06/07 12:23:45 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include "mlx/mlx.h"
#include "test2.h"
#include <math.h>
#include <stdlib.h>

/*void            my_pixel_put(t_img *img, int x, int y, int color)
{
    char    *dst;

    dst = img->data + (y * img->size_length + x * (img->bpp / 8));
    *(int *)dst = color;
}*/

/*int	close_win(int keycode, t_mlx *mlx)
{
	mlx_destroy_window(mlx->mlx_ptr, mlx->win_ptr);
//	return (1);
}*/

/*int	get_keycode(int keycode, t_mlx *mlx)
{
	printf("%d\n", keycode);
	return (1);
}*/

/*int	get_mousepos(int button, int x,int y, void *param)
{
	printf("mouse button: %d\n", button);
	printf("mouse x pos: %d\n", x);
	printf("mouse y pos: %d\n", y);
	return (1);
}*/

/*int	get_motion_mouse(int x,int y)
{
	printf("mouse x pos: %d\n", x);
	printf("mouse y pos: %d\n", y);
	return (1);
}*/

/*int		mouse_int_out(int x, int y)
{
	if ((x >= 0 && x <= WIDTH) && (y >= 0 && y <= HEIGHT))
		printf("Hello!\n");
	else
		printf("Bye!\n");
	return (1);
}*/

/*int		press_esc(int keycode, t_mlx *mlx)
{
	if (keycode == KEY_ESC)
	{
		mlx_destroy_image(mlx->mlx_ptr, mlx->img.img);
		mlx_destroy_window(mlx->mlx_ptr, mlx->win_ptr);
		exit(0);
		return (0);
	}
	else
		return (1);	
}*/

/*int	red_cross(t_mlx *mlx)
{
	mlx_destroy_image(mlx->mlx_ptr, mlx->img.img);
	mlx_destroy_window(mlx->mlx_ptr, mlx->win_ptr);
	printf("yeaaaaaaaaah\n");
	exit(0);
	return (0);
}*/

int	main(void)
{
	t_mlx	mlx;
	int count_x = -1;
	int count_y = -1;
	char *str_color = "jdsf     //255,255,255";
	int color = get_color_str(str_color);

	mlx.mlx_ptr = mlx_init();
	mlx.win_ptr = mlx_new_window(mlx.mlx_ptr, WIDTH, HEIGHT, "COUCOU");
	mlx.img.img = mlx_new_image(mlx.mlx_ptr, WIDTH, HEIGHT);
	mlx.img.data = (int *)mlx_get_data_addr(mlx.img.img, &mlx.img.bpp, &mlx.img.size_length, &mlx.img.endian);
/*	while (++count_x < 10)												//dessin carre
			mlx.img.data[(count_y * WIDTH + count_x )] = 0x00FFFFFF;
	while (++count_y < count_x)
			mlx.img.data[(count_y * WIDTH + count_x )] = 0x00FFFFFF;
	while (--count_x > 0)
			mlx.img.data[(count_y * WIDTH + count_x )] = 0x00FFFFFF;
	while (--count_y > count_x)
			mlx.img.data[(count_y * WIDTH + count_x )] = 0x00FFFFFF;*/
	while (++count_x < WIDTH)											//remplissage
	{
		while (++count_y < HEIGHT)
			mlx.img.data[(count_y * WIDTH + count_x )] = color;
		count_y = 0;
	}
	mlx_put_image_to_window(mlx.mlx_ptr, mlx.win_ptr, mlx.img.img, 0, 0);
//	mlx_key_hook(mlx.win_ptr, get_keycode, &mlx);
//	mlx_mouse_hook(mlx.win_ptr, get_mousepos, &mlx);
//	mlx_hook(mlx.win_ptr, 6, (1L<<6), get_motion_mouse, &mlx);
//	mlx_hook(mlx.win_ptr, 2, (1L<<0), press_esc, &mlx);
	//mlx_key_hook(mlx.win_ptr, close_win, &mlx);
//	mlx_hook(mlx.win_ptr, 17, (1L<<17), red_cross, &mlx);
//	mlx_hook(mlx.win_ptr, 6, (1L<<6), mouse_int_out, &mlx);
	mlx_loop(mlx.mlx_ptr);
}
