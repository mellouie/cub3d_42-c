/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/07 15:11:50 by mdesfont          #+#    #+#             */
/*   Updated: 2020/06/16 20:32:11 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"
#define TILE_SIZE			16

int		map[MAP_WIDTH][MAP_HEIGHT] =
{
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1},
	{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1},
	{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1},
	{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
	{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};

/*
** void	check_args(int argc, char **argv)
** {
** 	if (argc < 2 || argc > 3)
** 	{
** 		ft_putstr_fd(NB_ARGS, 2);
** 		exit(EXIT_FAILURE);
** 	}
** 	if (argc == 3 && ft_strcmp(argv[2], "--save"))
** 	{
** 		ft_putstr_fd(SAVE_ARG, 2);
** 		exit(EXIT_FAILURE);
** 	}
** }
*/

void	draw_squares(t_mlx *mlx, int x, int y)
{
	int		index_x;
	int		index_y;

	index_x = 0;
	x *= TILE_SIZE;
	y *= TILE_SIZE;
	while (index_x < TILE_SIZE)
	{
		index_y = 0;
		while (index_y < TILE_SIZE)
		{
			mlx->img.data[(y + index_x) * WIDTH + x + index_y] = 0x00FFFFFF;
			index_y++;
		}
		index_x++;
	}
}

void	parse_squares(t_mlx *mlx)
{
	int		count_x;
	int		count_y;

	count_x = 0;
	while (count_x < MAP_WIDTH)
	{
		count_y = 0;
		while (count_y < MAP_HEIGHT)
		{
			if (map[count_x][count_y] == 1)
				draw_squares(mlx, count_x, count_y);
			count_y++;
		}
		count_x++;
	}
}

int		loop(t_mlx *mlx)
{
	parse_squares(mlx);
	mlx_put_image_to_window(mlx->mlx, mlx->win, mlx->img.ptr, 0, 0);
	return (0);
}

int		setup_settings(t_mlx *mlx)
{
	mlx->mlx = mlx_init();
	mlx->win = mlx_new_window(mlx->mlx, WIDTH, HEIGHT, WIN_NAME);
	mlx->img.ptr = mlx_new_image(mlx->mlx, WIDTH, HEIGHT);
	mlx->img.data = (int *)mlx_get_data_addr(mlx->img.ptr, &mlx->img.bpp,
					&mlx->img.size_l, &mlx->img.endian);
	return (0);
}

int		main(void)
{
	t_mlx	mlx;
	//check_args(argc, argv);
	setup_settings(&mlx);
//	if (argc == 3)
		//screenshot();
	mlx_hook(mlx.win, 2, (1L << 0), moves, &mlx);
	mlx_hook(mlx.win, 17, (1L << 17), close_and_exit, &mlx);
	mlx_loop_hook(mlx.mlx, &loop, &mlx);
	mlx_loop(mlx.mlx);
	return (0);
}
