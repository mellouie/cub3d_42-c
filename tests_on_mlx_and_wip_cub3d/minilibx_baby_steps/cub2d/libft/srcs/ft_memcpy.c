/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 11:42:19 by mdesfont          #+#    #+#             */
/*   Updated: 2019/11/19 16:15:59 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char	*m;
	char	*l;

	if ((dst == NULL) && (src == NULL))
		return (NULL);
	m = (char*)dst;
	l = (char*)src;
	while (n--)
		*m++ = *l++;
	return (dst);
}
