/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/08 14:47:20 by mdesfont          #+#    #+#             */
/*   Updated: 2020/06/16 20:32:11 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int		close_and_exit(t_mlx *mlx)
{
	mlx_destroy_image(mlx->mlx, mlx->img.ptr);
	mlx_destroy_window(mlx->mlx, mlx->win);
	ft_putstr_fd("Everything went well!\n", 1);
	exit(0);
	return (0);
}

int		moves(int keycode, t_mlx *mlx)
{
	if (keycode == KEY_ESC)
		close_and_exit(mlx);
	return (0);
}
