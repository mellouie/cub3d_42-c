/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/08 11:48:16 by mdesfont          #+#    #+#             */
/*   Updated: 2020/06/08 16:40:09 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H
# include <stdlib.h>
# include "libft/includes/libft.h"
# include "mlx/mlx.h"
# include "error_codes.h"
# include "key_codes.h"

//# define WIN_WIDTH 640
//# define WIN_HEIGHT 480
# define WIN_NAME "cub3d"
# define MAP_WIDTH 11
# define MAP_HEIGHT 15
#define WIDTH MAP_WIDTH * TILE_SIZE
#define HEIGHT MAP_HEIGHT * TILE_SIZE

typedef	unsigned int byte;

/*typedef struct	s_game
{
	
}				t_game;*/

typedef struct	s_img
{
	void	*ptr;
	int	*data;
	int		bpp;
	int		size_l;
	int		endian;
}				t_img;

typedef struct	s_mlx
{
	void	*mlx;
	void	*win;
	t_img	img;
}				t_mlx;

int				key_press(int keycode, t_mlx *mlx);
int				close_and_exit(t_mlx *mlx);
#endif
