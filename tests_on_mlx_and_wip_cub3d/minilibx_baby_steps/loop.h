#ifndef LOOP_H
# define LOOP_H

# define WIDTH 600
# define HEIGHT 400
# define WIN_NAME "hello"
# define KEY_ESC 53

//typedef unsigned char t_byte;

typedef struct	s_img
{
	void	*ptr;
	int		*data;
	int		bpp;
	int		size_length;
	int		endian;
}				t_img;

typedef struct	s_mlx
{
	void	*mlx;
	void	*win;
	t_img	img;
}				t_mlx;

t_img	get_img(void *mlx);
int		img_loop(t_mlx *mlx);
//static void	draw_splash(t_mlx *mlx);
#endif