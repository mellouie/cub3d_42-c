/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main-backup.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdesfont <mdesfont@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/08 11:48:16 by mdesfont          #+#    #+#             */
/*   Updated: 2020/07/07 11:10:25 by mdesfont         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# ifndef MAIN_H
# define MAIN_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "../mlx/mlx.h"
#include "../error_codes.h"
#include "../key_codes.h"
#include "../libft/includes/libft.h"

# define WIN_WIDTH 640
# define WIN_HEIGHT 480
# define TEX_WIDTH 64
# define TEX_HEIGHT 64

# define MIN_WIN_WIDTH 320
# define MIN_WIN_HEIGHT 240
# define MAX_WIN_WIDTH 1280
# define MAX_WIN_HEIGHT 720

# define WIN_NAME "cub3d"

#define mapWidth 24
#define mapHeight 24

int mlx->map[mapWidth][mapHeight];

enum	e_direction
{
	EAST, WEST, SOUTH, NORTH, SPRITE
};


typedef struct      s_img
{
	void				   *img_ptr;
	int     *data;
	int     size_l;
	int     bpp;
	int     endian;
}                   t_img;

typedef struct		s_tex
{
	void	*img_ptr;
	int		*data;
	int     size_l;
	int     bpp;
	int     endian;
	int		width;
	int		height;
	char	*filepath;
}					t_tex;

typedef struct		s_game
{
	double	posx;
	double	posy;
	double	dirx;
	double	diry;
	double	planex;
	double	planey;
	int		mapx;
	int 	mapy;
	double	sidedistx;
	double	sidedisty;
	double	deltadistx;
	double	deltadisty;
	double	perpWallDist;
	int		stepx;
	int		stepy;
	int		hit;
	int		side;
	double	camerax;
	double	raydirx;
	double	raydiry;
	int		drawStart;
	int		drawEnd;
	int		lineheight;
	int		move_f;
	int		move_b;
	int		move_r;
	int		move_l;
	int		rotate_r;
	int		rotate_l;
}					t_game;

typedef struct		s_sprite
{
	double		x;
	double		y;
	double		spriteX;
	double		spriteY;
	double		transformX;
	double		transformY;
	double		spriteScreenX;
	int			perpWallDist;
	double		dist;
	double		spriteHeight;
	double		spriteWidth;
	int			drawStartX;
	int			drawEndX;
	int			drawStartY;
	int			drawEndY;
}					t_sprite;


typedef struct		s_mlx
{
	void		*mlx_ptr;
	void		*win_ptr;
	t_img		img;
	t_game		game;
	t_tex		tex[5];
	t_sprite	*sprite;
	double		zbuffer[MAX_WIN_WIDTH];
	int			spritenum;
	int			winWidth;
	int			winHeight;
}					t_mlx;

int					close_and_exit(t_mlx *mlx);
int					key_release(int key, t_mlx *mlx);
int					key_press(int key, t_mlx *mlx);
int					moves(t_mlx *mlx);
int					set_side_dist(t_mlx *mlx);
int					set_var(t_mlx *mlx, int i);
void				set_draw(t_mlx *mlx);
void				perform_dda(t_mlx *mlx);
int					get_side(t_mlx *mlx);
void				draw_vert_line_tex(t_mlx *mlx, int i);
void				draw_floor_ceiling(t_mlx *mlx);
void				tmp_direction_tex(t_mlx *mlx);
void				render_sprite(t_mlx *mlx);
void				desc_sort(t_mlx *mlx);
void				transform_sprite(t_game *game, t_sprite *sprite);
void				draw_sprite(t_mlx *mlx, t_sprite *sprite);
void				cal_sprite(t_sprite *sprite);
//void				check_args(int argc, char **argv);
# endif
